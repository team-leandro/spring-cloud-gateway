package br.dev.leandro.spring.gateway;

import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@Slf4j
@RefreshScope
@SpringBootApplication
public class SpringCloudApiGatewayApplication {

    public static void main(String[] args) {
        log.info(":: Iniciando Spring-Api-Gateway ::");
        SpringApplication.run(SpringCloudApiGatewayApplication.class, args);
        log.info(":: Spring-Api-Gateway iniciado com sucesso ::");
    }
}
